﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

namespace Completed {

	public class GameManager : MonoBehaviour {

		public float levelStartDelay = 2f;
		public float turnDelay = 0.5f;
		private static GameManager instance = null;
		public static GameManager Instance { get { return instance; } }
		[HideInInspector] public bool playersTurn = true;

		private Text levelText;
		private GameObject levelImage;
		private BoardManager boardScript;
		private int level = 0;
		private List<Enemy> enemies;
		private bool enemiesMoving;
		private bool doingSetup = true;

		void Awake() {
			if (instance != null && instance != this) {
				Destroy (this.gameObject);
				return;
			}
			else {
				instance = this;
			}

			DontDestroyOnLoad (this.gameObject);
			enemies = new List<Enemy> ();
			boardScript = GetComponent<BoardManager> ();

		}

		void OnLevelFinishLoading(Scene scene, LoadSceneMode mode) {
			level++;
			InitGame ();
		}

		void OnEnable() {
			SceneManager.sceneLoaded += OnLevelFinishLoading;
		}

		void InitGame () {
			doingSetup = true;
			levelImage = GameObject.Find ("LevelImage");
			levelText = GameObject.Find ("LevelText").GetComponent<Text> ();
			levelText.text = "Day " + level;
			levelImage.SetActive (true);
			Invoke ("HideLevelImage", levelStartDelay);
			enemies.Clear ();
			boardScript.SetupScene(level);

			// EndGame "You Got Caught Your murder spree is Over!!"
		}

		void HideLevelImage() {
			levelImage.SetActive (false);
			doingSetup = false;
		}

		// Update is called once per frame
		void Update () {
			if (playersTurn || enemiesMoving || doingSetup) {
				return;
			}

			StartCoroutine (MoveEnemies ());
		}

		public void AddEnemyToList(Enemy script) {
			enemies.Add (script);
		}

		public void GameOver() {
			levelText.text = "You Have Been Caught!! Your Murder Spree is over!";
			levelImage.SetActive (true);
			enabled = false;
		}

		IEnumerator MoveEnemies() {
			enemiesMoving = true;

			yield return new WaitForSeconds (turnDelay);

			if (enemies.Count == 0) {
				yield return new WaitForSeconds (turnDelay);
			}

			for (int i = 0; i < enemies.Count; i++) {
				enemies [i].MoveEnemy ();

				yield return new WaitForSeconds (enemies[i].moveTime);
			}
			playersTurn = true;
			enemiesMoving = false;
		}
	}		
}
