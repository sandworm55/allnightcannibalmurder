﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class StartButtom : MonoBehaviour {
	
	private Color original;

	void Awake() {
		original = gameObject.GetComponent<GUITexture> ().color;
	}

	void OnMouseOver() {
		if (Input.GetMouseButton(0)) {
			gameObject.GetComponent<GUITexture> ().color = Color.white;

			SceneManager.LoadScene (1);
		}
		else {
			gameObject.GetComponent<GUITexture>().color = Color.yellow;
		}
	}

	void OnMouseExit() {
		gameObject.GetComponent<GUITexture> ().color = original;
	}
		
}
